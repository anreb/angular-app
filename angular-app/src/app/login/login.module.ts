import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { ViewComponent } from './view/view.component';

@NgModule({
	declarations: [ ViewComponent ],
	imports: [ CommonModule, LoginRoutingModule, ReactiveFormsModule ]
})
export class LoginModule {
	constructor() {
		console.log('Login module loaded');
	}
}
