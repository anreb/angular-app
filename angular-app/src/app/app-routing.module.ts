import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PostResolver} from './shared/post.resolver';
import {PostDetailResolver} from './shared/post-detail.resolver';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'signup',
    loadChildren: () =>
      import('./signup/signup.module').then(m => m.SignupModule)
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./profile/profile.module').then(m => m.ProfileModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'post',
    loadChildren: () => import('./post/post.module').then(m => m.PostModule),
    resolve: [PostResolver],
    canActivate: [AuthGuard]
  },
  {
    path: 'post-detail/:id',
    loadChildren: () =>
      import('./post-detail/post-detail.module').then(m => m.PostDetailModule),
    resolve: { postData: PostDetailResolver },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    PostResolver,
    PostDetailResolver
  ]
})
export class AppRoutingModule {}
