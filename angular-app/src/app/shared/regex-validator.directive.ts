import {AbstractControl, ValidatorFn} from '@angular/forms';

export function regexValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = nameRe.test(control.value);
    // tslint:disable-next-line: object-literal-key-quotes
    return forbidden ? null : {'forbiddenName': {value: control.value}};
  };
}

// tslint:disable-next-line: max-line-length
export const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const phoneRegex = /^[+]?(1\-|1\s|1|\d{3}\-|\d{3}\s|)?((\(\d{3}\))|\d{3})(\-|\s)?(\d{3})(\-|\s)?(\d{4})$/g;
export const allLettersRegex = /^[A-Za-z]+$/;
export const allNumbersRegex = /^[0-9]+$/;






