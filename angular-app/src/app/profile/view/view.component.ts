import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup} from '@angular/forms';

import { regexValidator, emailRegex, phoneRegex, allLettersRegex, allNumbersRegex } from '../../shared/regex-validator.directive';
import { IndexeddbService } from '../../services/indexed-db.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  editForm: FormGroup;
  user = JSON.parse(localStorage.user);

  constructor(
    private fb: FormBuilder,
    private db: IndexeddbService,
  ) { }

  ngOnInit(): void {
    console.log(this.user);
    this.editForm = this.fb.group({
      email: [this.user.email, [regexValidator(emailRegex)]],
      phone: [this.user.phone, [regexValidator(phoneRegex)]],
      name: [this.user.name, [regexValidator(allLettersRegex)]],
      lastname: [this.user.lastname, [regexValidator(allLettersRegex)]],
      id: [this.user.id, [regexValidator(allNumbersRegex)]]
    });
  }

  editUser() {
    const editUser = this.db.editUser(this.editForm.value).subscribe(res => {
      res ? window.location.reload() : null;
    });
  }
}
