import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostDetailRoutingModule } from './post-detail-routing.module';
import { ViewComponent } from './view/view.component';


@NgModule({
  declarations: [ViewComponent],
  imports: [
    CommonModule,
    PostDetailRoutingModule
  ]
})
export class PostDetailModule { 
  constructor() {
    console.log('Post-detail module loaded');
  }
}
